﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        public static double Calculator(double num1, double num2, string op)
        {
            double result = double.NaN;
            switch (op)
            {
                case "a":
                    result = num1 + num2;
                    break;
                case "s":
                    result = num1 - num2;
                    break;
                case "m":
                    result = num1 * num2;
                    break;
                case "d":
                    while (num2 == 0)
                    {
                        Console.WriteLine("Enter a non-zero number");
                        num2 = Convert.ToDouble(Console.ReadLine());
                    }
                    result = num1 / num2;
                    break;
            }
            return result;
        }

        static void Main(string[] args)
        {
            bool endApp = false;
            while (!endApp)
            {
                double num1 = 0;
                double num2 = 0;
                double result = 0;

                Console.WriteLine("Console Calculator in C#\r");
                Console.WriteLine("-----------------------\n");

                Console.WriteLine("Please enter the first number");
                num1 = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Please enter another number");
                num2 = Convert.ToDouble(Console.ReadLine());

                //Ask the user to chose an option.
                Console.WriteLine("Choose an option from the following: ");
                Console.WriteLine("\ta - Add");
                Console.WriteLine("\ts - Substract");
                Console.WriteLine("\tm - Multiply");
                Console.WriteLine("\td - Divide");
                Console.WriteLine("Your option?");
                string op = Console.ReadLine();
                try
                {
                    result = Calculator(num1, num2, op);
                    Console.WriteLine(result);
                }
                catch(Exception e)
                {
                    Console.WriteLine("Oh no there is an error: " + e.Message);
                }
                Console.WriteLine("Press 'n' to close the Calculator console app...");
                if(Console.ReadLine() == "n") endApp = true;
                Console.WriteLine("\n");
            }
            return;
        }
    }
}
